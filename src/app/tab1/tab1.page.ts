import { Component } from '@angular/core'
import { CartService } from '../cart.service'
import { Product } from '../product'
import { ProductService } from '../product.service'

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss'],
})
export class Tab1Page {
  get products() {
    return this.productService.products
  }

  username = 'alice'

  constructor(
    private productService: ProductService,
    private cartService: CartService,
  ) {}

  addToCart(product: Product): void {
    this.cartService.addToCart(product)
  }

  getTime() {
    return new Date().toTimeString()
  }
}
