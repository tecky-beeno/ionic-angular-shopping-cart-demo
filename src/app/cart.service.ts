import { Injectable } from '@angular/core'
import { Product } from './product'
import { ProductService } from './product.service'

@Injectable({
  providedIn: 'root',
})
export class CartService {
  productsInCart: Product[] = []
  total = 0

  constructor() {}

  addToCart(product: Product): void {
    product.cart++
    this.total += product.price
    if (product.cart == 1) {
      this.productsInCart.push(product)
    }
  }
}
