import { Injectable } from '@angular/core'
import { Product } from './product'

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  constructor() {}

  products: Product[] = [
    {
      image:
        'https://www.collinsdictionary.com/images/full/apple_158989157.jpg',
      name: 'single apple',
      price: 10,
      cart: 0,
    },
    {
      image:
        'https://cdn.britannica.com/47/103847-050-8E18B710/varieties-apples.jpg',
      name: 'multiple apples',
      price: 100,
      cart: 0,
    },
    {
      image:
        'https://www.apple.com/v/apple-watch-series-7/d/images/overview/connect/connect_hw__cryw5lqu4ryq_large.jpg',
      name: 'thin apple',
      price: 880,
      cart: 0,
    },
  ]
}
