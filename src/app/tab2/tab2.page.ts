import { Component } from '@angular/core'
import { CartService } from '../cart.service'

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss'],
})
export class Tab2Page {
  get products() {
    return this.cartService.productsInCart
  }

  constructor(private cartService: CartService) {}

  get total() {
    return this.cartService.total
  }
}
